// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });



import React, { useState } from "react";
import { Text, View, TextInput, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { styles } from "./styles/AppStyles";

export default function App() {
  const [todos, setTodos] = useState([]);
  const [inputText, setInputText] = useState("");
  const [editingIndex, setEditingIndex] = useState(-1);

  const addTodo = () => {
    if (inputText.trim() !== "") {
      setTodos([...todos, inputText]);
      setInputText("");
    }
  };

  const removeTodo = (index) => {
    const newTodos = todos.filter((_, todoIndex) => todoIndex !== index);
    setTodos(newTodos);
    // Если удаляем элемент во время редактирования, отменяем редактирование
    if (editingIndex === index) {
      setEditingIndex(-1);
    }
  };

  const updateTodo = (index, newText) => {
    const updatedTodos = [...todos];
    updatedTodos[index] = newText;
    setTodos(updatedTodos);
    setEditingIndex(-1); // Завершаем редактирование после обновления текста
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Todo List</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Enter todo"
          value={inputText}
          onChangeText={(text) => setInputText(text)}
        />
        <TouchableOpacity style={styles.addButton} onPress={addTodo}>
          <Text style={styles.buttonText}>Add</Text>
        </TouchableOpacity>
      </View>
      {todos.map((todo, index) => (
        <View key={index} style={styles.todoContainer}>
          {editingIndex === index ? (
            <TextInput
              style={styles.editInput}
              value={todo}
              onChangeText={(text) => updateTodo(index, text)}
              autoFocus
              onBlur={() => setEditingIndex(-1)}
            />
          ) : (
            <Text style={styles.todoText}>{todo}</Text>
          )}
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={() => removeTodo(index)}>
              <Text style={styles.deleteButton}>
                <Icon name="trash" size={20} color="#333" />
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setEditingIndex(index)}>
              <Text style={styles.editButton}>
                <Icon name="edit" size={20} color="#333" />
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      ))}
    </View>
  );
}

